package com.mr_mysterium.ElevatorPlugin.model.elevator;

import java.util.ArrayList;
import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarStyle;
import org.bukkit.boss.BossBar;
import org.bukkit.entity.Player;

public class Passenger {

	static HashMap<Player,Passenger>passengers= new HashMap<Player,Passenger>();
	
	private BossBar floorbar;
	private final Elevator elevator;
	private int floornumber;
	
	public Passenger(Elevator elevator,int floornumber) {
		this.elevator=elevator;
		this.floornumber=floornumber;
		this.floorbar=Bukkit.createBossBar("Floor "+(floornumber+1)+" of "+ elevator.getFloors().size(), BarColor.PURPLE, BarStyle.SOLID);
		floorbar.setVisible(true);
		floorbar.setProgress(0);
	}

	public int getFloornumber() {
		return floornumber;
	}

	public void setFloornumber(int floornumber) {
		this.floornumber = floornumber;
		floorbar.setTitle("Floor "+(floornumber+1)+" of "+ elevator.getFloors().size());
		floorbar.setProgress((float)(floornumber+1)/(float)elevator.getFloors().size());
		
		
	}

	public Elevator getElevator() {
		return elevator;
	}

	public BossBar getFloorbar() {
		return floorbar;
	}

	public static HashMap<Player, Passenger> getPassengers() {
		return passengers;
	}
	
	
	
	
}
