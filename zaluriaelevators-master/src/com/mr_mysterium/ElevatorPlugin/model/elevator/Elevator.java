package com.mr_mysterium.ElevatorPlugin.model.elevator;

import java.util.ArrayList;
import java.util.HashMap;

import org.bukkit.Location;
import org.bukkit.entity.Player;

public class Elevator {
	static ArrayList<Elevator>elevators= new ArrayList<Elevator>();
	
	ArrayList<Floor> floors = new ArrayList<Floor>();
	Floor origin;
	
	public Elevator(Location location) {
		Floor origin= new Floor(location);
		floors.add(origin);
		this.origin=origin;
	}

	public ArrayList<Floor> getFloors() {
		return floors;
	}

	public void addFloor(Floor newfloor) {
		for (int i=0;i<floors.size();i++) {
			if (newfloor.getLocation().getBlockY()>floors.get(i).getLocation().getBlockY()) {
				continue;
			}else {
				floors.add(i,newfloor);
				return;
			}
		}
		floors.add(newfloor);
	}
	
	public void removeFloor (Floor removedfloor) {
		floors.remove(removedfloor);
	}
	
	public void setOrigin(Floor origin) {
		this.origin = origin;
	}

	public Floor getOrigin() {
		return origin;
	}

	public static ArrayList<Elevator> getElevators() {
		return elevators;
	}
	
	
	
}
