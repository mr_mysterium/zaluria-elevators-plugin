package com.mr_mysterium.ElevatorPlugin.model.handler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import org.bukkit.ChatColor;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockExplodeEvent;
import org.bukkit.event.block.BlockPistonExtendEvent;
import org.bukkit.event.block.BlockPistonRetractEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerToggleSneakEvent;

import com.mr_mysterium.ElevatorPlugin.model.elevator.Elevator;
import com.mr_mysterium.ElevatorPlugin.model.elevator.Floor;
import com.mr_mysterium.ElevatorPlugin.model.elevator.Passenger;

public class EventHandler implements Listener {

	ArrayList<Elevator>elevators= Elevator.getElevators();
	HashMap<Player,Passenger>passengers= Passenger.getPassengers();
	DatabaseHandler dbhandler;
	
	
	public EventHandler(DatabaseHandler dbhandler) {
		this.dbhandler= dbhandler;
	}
	
	
	
	@org.bukkit.event.EventHandler
	public void onStep(PlayerInteractEvent event) {

		if (event.getAction() != Action.PHYSICAL)
			return;
		Player player = event.getPlayer();

		if (player.getWorld().getBlockAt(event.getClickedBlock().getLocation())
				.getType() != Material.LIGHT_WEIGHTED_PRESSURE_PLATE)
			return;
		if (player.getWorld().getBlockAt(event.getClickedBlock().getLocation().subtract(0, 1, 0))
				.getType() != Material.REDSTONE_BLOCK)
			return;

		for (int i = 0; i < elevators.size(); i++) {
			Block originblock = elevators.get(i).getOrigin().getLocation().getBlock();
			Block eventblock = event.getClickedBlock();
			if (eventblock.getX() != originblock.getX() || eventblock.getZ() != originblock.getZ()) {

				continue;
			}

			ArrayList<Floor> floors = elevators.get(i).getFloors();

			for (int j = 0; j < floors.size(); j++) {
				Block floorblock = floors.get(j).getLocation().getBlock();
				if (eventblock.getX() != floorblock.getX() || eventblock.getY() != floorblock.getY()
						|| eventblock.getZ() != floorblock.getZ()) {
					continue;
				}

				if (!passengers.containsKey(player)) {
					passengers.put(player, new Passenger(elevators.get(i), j));
					passengers.get(player).setFloornumber(j);
					passengers.get(player).getFloorbar().addPlayer(player);
				}
			}
		}

	}
	
	@org.bukkit.event.EventHandler
	public void onMove(PlayerMoveEvent event) {
		if(!passengers.containsKey(event.getPlayer()))return;
		Player player= event.getPlayer();
		
		Elevator elevator= passengers.get(player).getElevator();
		Floor floor= elevator.getFloors().get(passengers.get(player).getFloornumber());
		Block floorblock= floor.getLocation().getBlock();
		Location blockmiddle= floorblock.getLocation().add(0.5,0,0.5);
		double distx=Math.abs(blockmiddle.getX()-player.getLocation().getX());
		double distz=Math.abs(blockmiddle.getZ()-player.getLocation().getZ());
		if(distx>1||distz>1) {
		   passengers.get(player).getFloorbar().removeAll();
		   passengers.remove(event.getPlayer());
		   return;
		}
		if (player.getVelocity().getY()>0&&passengers.get(player).getFloornumber()<elevator.getFloors().size()-1) {

			player.teleport(new Location(player.getWorld(), 0.5, 0, 0.5,player.getLocation().getYaw(),player.getLocation().getPitch()).add(elevator.getFloors().get(passengers.get(player).getFloornumber()+1).getLocation()));
			player.getWorld().spawnParticle(Particle.CRIT, player.getLocation(), 20);
			player.getWorld().playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1f, 1f);
			passengers.get(player).setFloornumber(passengers.get(player).getFloornumber()+1);
			player.sendTitle(ChatColor.GREEN+"Up","", 10, 20, 10);
		}	
	}
	
	@org.bukkit.event.EventHandler
	public void onsneak(PlayerToggleSneakEvent event) {
		if(!passengers.containsKey(event.getPlayer()))return;
		Player player= event.getPlayer();
		if (!player.isSneaking()&&passengers.get(player).getFloornumber()>0) {
			Elevator elevator= passengers.get(player).getElevator();
			player.teleport(new Location(player.getWorld(), 0.5, 0, 0.5,player.getLocation().getYaw(),player.getLocation().getPitch()).add(elevator.getFloors().get(passengers.get(player).getFloornumber()-1).getLocation()));
			player.getWorld().spawnParticle(Particle.CRIT, player.getLocation(), 20);
			player.getWorld().playSound(player.getLocation(), Sound.BLOCK_NOTE_BLOCK_BASS, 1f, 1f);
			passengers.get(player).setFloornumber(passengers.get(player).getFloornumber()-1);
			player.sendTitle(ChatColor.RED+"Down","", 10, 20, 10);
		}	
	}

	@org.bukkit.event.EventHandler
public void onBreak(BlockBreakEvent event) {
		
		Block broken= event.getBlock();
		if (broken.getType()!= Material.LIGHT_WEIGHTED_PRESSURE_PLATE &&
			broken.getType()!= Material.REDSTONE_BLOCK)return;
		
		Player player= event.getPlayer();
		Location location;
		
		if   (broken.getType()==Material.LIGHT_WEIGHTED_PRESSURE_PLATE)location = broken.getLocation();
		else location = broken.getLocation().add(0,1,0);
		
		
		
		for (int i = 0; i < elevators.size(); i++) {
			
			 Location originLocation= elevators.get(i).getOrigin().getLocation();
			 if (location.getBlockX()==originLocation.getBlockX()&&location.getBlockZ()==originLocation.getBlockZ()) {
				 ArrayList<Floor> floors= elevators.get(i).getFloors();
				
				 for (int j = 0; j <floors.size(); j++) {
					 	if (floors.get(j).getLocation().toVector().equals(location.toVector())) {
					 	
					 	if(passengers.get(player)!=null)passengers.get(player).getFloorbar().removeAll();
						elevators.get(i).removeFloor(floors.get(j));
						passengers.remove(player);
						player.sendMessage("Removed floor number: "+(j+1));
						dbhandler.saveFloor(elevators.get(i));
						
						if (floors.size()==0) {
							dbhandler.removeElevator(elevators.get(i));
							elevators.remove(i);
							passengers.remove(player);
							player.sendMessage("Removed Elevator");
						
						}else if (j==0) {
							elevators.get(i).setOrigin(floors.get(0));
						}
					 }	
				 }
				 return;
			 }
		}	
	}
	
	@org.bukkit.event.EventHandler
	public void onPlace(BlockPlaceEvent event) {
		
		Block placed= event.getBlockPlaced();
		if (placed.getType()!= Material.LIGHT_WEIGHTED_PRESSURE_PLATE)return;
		
		
		Location location = placed.getLocation();
		if (location.getWorld().getBlockAt((int)location.getX(),(int)location.getY()-1,(int)location.getZ()).getType()!=Material.REDSTONE_BLOCK)return; 
		
		Player player= event.getPlayer();
		if (!player.hasPermission("elevator.create")) {
			player.sendMessage(ChatColor.RED+"You need to be a lvl 60 builder to create elevators");
			return;
		}
		
		for (int i = 0; i < elevators.size(); i++) {
			 Location originLocation= elevators.get(i).getOrigin().getLocation();
			if (location.getBlockX()==originLocation.getBlockX()&&location.getBlockZ()==originLocation.getBlockZ()) {
				elevators.get(i).addFloor(new Floor(location));
				dbhandler.saveFloor(elevators.get(i));
				player.sendMessage("Floor created");
				player.sendMessage("Total Floors in Elevator: "+elevators.get(i).getFloors().size());
				return;
			}
		}
		Elevator elevator = new Elevator(location);
		elevators.add(elevator);
		dbhandler.saveElevator(elevator.getOrigin().getLocation());
		player.sendMessage("Elevator created");
		
	}
	
	@org.bukkit.event.EventHandler
	public void onexplode(EntityExplodeEvent event) {

		Iterator<Block> it = event.blockList().iterator();

		while(it.hasNext()){
			
		            Block block = it.next();
		            
		            
		            if (block.getType()==Material.LIGHT_WEIGHTED_PRESSURE_PLATE&&
		            	block.getLocation().subtract(0,1,0).getBlock().getType()==Material.REDSTONE_BLOCK) {
		                it.remove();
		            }
		            
		            if (block.getType()==Material.REDSTONE_BLOCK&&
		            	block.getLocation().add(0,1,0).getBlock().getType()==Material.LIGHT_WEIGHTED_PRESSURE_PLATE) {
		                it.remove();
		            }
		}
	}
	
	@org.bukkit.event.EventHandler
	public void onexplode(BlockExplodeEvent event) {

		Iterator<Block> it = event.blockList().iterator();

		while(it.hasNext()){
			
		            Block block = it.next();
		            
		            
		            if (block.getType()==Material.LIGHT_WEIGHTED_PRESSURE_PLATE&&
		            	block.getLocation().subtract(0,1,0).getBlock().getType()==Material.REDSTONE_BLOCK) {
		                it.remove();
		            }
		            
		            if (block.getType()==Material.REDSTONE_BLOCK&&
		            	block.getLocation().add(0,1,0).getBlock().getType()==Material.LIGHT_WEIGHTED_PRESSURE_PLATE) {
		                it.remove();
		            }
		}
	}
	
	@org.bukkit.event.EventHandler  
	public void onPistonEvent(BlockPistonExtendEvent e)
	{      
	    for (Block block : e.getBlocks()) {
	    	if (block.getType()==Material.LIGHT_WEIGHTED_PRESSURE_PLATE&&
	            	block.getLocation().subtract(0,1,0).getBlock().getType()==Material.REDSTONE_BLOCK) {
	               	e.setCancelled(true);
	            }
	            
	            if (block.getType()==Material.REDSTONE_BLOCK&&
	            	block.getLocation().add(0,1,0).getBlock().getType()==Material.LIGHT_WEIGHTED_PRESSURE_PLATE) {
	                e.setCancelled(true);
	            }
		}
	}  
	
	@org.bukkit.event.EventHandler  
	public void onPistonEvent(BlockPistonRetractEvent e)
	{      
		Block block = e.getBlock();
		if (block.getType()==Material.LIGHT_WEIGHTED_PRESSURE_PLATE&&
            	block.getLocation().subtract(0,1,0).getBlock().getType()==Material.REDSTONE_BLOCK) {
               	e.setCancelled(true);
            }
            
            if (block.getType()==Material.REDSTONE_BLOCK&&
            	block.getLocation().add(0,1,0).getBlock().getType()==Material.LIGHT_WEIGHTED_PRESSURE_PLATE) {
                e.setCancelled(true);
            }
	}
}
