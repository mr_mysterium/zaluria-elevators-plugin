package com.mr_mysterium.ElevatorPlugin.model.handler;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.configuration.Configuration;

import com.mr_mysterium.ElevatorPlugin.ZaluriaElevators;
import com.mr_mysterium.ElevatorPlugin.model.elevator.Elevator;
import com.mr_mysterium.ElevatorPlugin.model.elevator.Floor;
import com.zaxxer.hikari.HikariDataSource;

public class DatabaseHandler {
	Configuration config;
	private HikariDataSource hikari;
	
	public DatabaseHandler(Configuration config) {
		this.config=config;
	}
	
	public void connectToDatabase() {
		String address = config.getString("Database.Address");
		String name = config.getString("Database.Name");
		String username = config.getString("Database.Username");
		String password = config.getString("Database.Password");
		
		hikari = new HikariDataSource();
		hikari.setMaximumPoolSize(10);
		hikari.setDataSourceClassName("com.mysql.jdbc.jdbc2.optional.MysqlDataSource");
		hikari.addDataSourceProperty("serverName", address);
		hikari.addDataSourceProperty("port", 3306);
		hikari.addDataSourceProperty("databaseName", name);
		hikari.addDataSourceProperty("user", username);
		hikari.addDataSourceProperty("password", password);
		hikari.addDataSourceProperty("UseSSL", false);
		hikari.addDataSourceProperty("AllowPublicKeyRetrieval", true);
		
		Connection connection = null;
		String table = "CREATE TABLE IF NOT EXISTS `elevator` (`id` int(11) NOT NULL AUTO_INCREMENT,"
				+ "`world` varchar(64) NOT NULL,"
				+ "`origin` varchar(45) NOT NULL,"
				+ "`floors` varchar(1024) DEFAULT NULL ,"
				+ " PRIMARY KEY (`id`))";
        PreparedStatement p = null;
		
		try {
			connection= hikari.getConnection();
			p =  connection.prepareStatement(table);
			p.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			if (connection!=null) {
				try {
				connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			
			if(p!=null) {
				try {
					p.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		
	}
	
	public void loadElevators() {
		
		Connection connection = null;
		String elevatorquery= "SELECT world,origin,floors FROM elevator";
		PreparedStatement p = null;
		
		try {
			connection= hikari.getConnection();
			p =  connection.prepareStatement(elevatorquery);
			ResultSet rs = p.executeQuery();
			while (rs.next()) {
				String world=rs.getString("world");
				
				String[] origin=rs.getString("origin").split(",");
				
				String[] floors=rs.getString("floors").split(",");
				int x = Integer.parseInt(origin[0]);
				int y = Integer.parseInt(floors[0]);
				int z = Integer.parseInt(origin[1]);
				Location location = new Location(Bukkit.getWorld(world), x,y,z);
				Location below= new Location(Bukkit.getWorld(world), x, y-1, z);
				
				Elevator e = new Elevator(location);
				Elevator.getElevators().add(e);
				
				for (int i=1;i<floors.length;i++) {
				Double floory= Double.parseDouble(floors[i]);
				Location floorlocation = new Location(Bukkit.getWorld(world), x,floory,z);
				e.addFloor(new Floor(floorlocation));	
				}
				
				for (int i = 0; i < e.getFloors().size(); i++) {
					if (location.getBlock().getType()!=Material.LIGHT_WEIGHTED_PRESSURE_PLATE||
						below.getBlock().getType() != Material.REDSTONE_BLOCK) {
						e.removeFloor(e.getFloors().get(i));
					}
				}
				if (e.getFloors().size()>0) {
					
					saveFloor(e);
				}else {
					removeElevator(e);
				}
				
				
			}
				
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			if (connection!=null) {
				try {
				connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			
			if(p!=null) {
				try {
					p.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		
	}
	
	
	public void saveElevator(Location location){
		
		Connection connection = null;
		String update = "INSERT INTO elevator (world,origin,floors) VALUES (?, ?,?)";
        PreparedStatement p = null;
		
		try {
			connection= hikari.getConnection();
			p =  connection.prepareStatement(update);
			p.setString(1, location.getWorld().getName());
			Block originblock= location.getBlock();
			p.setString(2, originblock.getX()+","+originblock.getZ());
			p.setString(3,originblock.getY()+",");
			p.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			if (connection!=null) {
				try {
				connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			
			if(p!=null) {
				try {
					p.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		
	}
	
public void saveFloor(Elevator elevator){
		
		Connection connection = null;
		String update = "UPDATE elevator" + " SET floors = ? "+"WHERE origin = ? AND world = ?" ;
        PreparedStatement p = null;
		
		try {
			String floors="";
			for (Floor f : elevator.getFloors()) {
			floors+= f.getLocation().getBlockY()+",";	
			}
			connection= hikari.getConnection();
			p =  connection.prepareStatement(update);
			p.setString(1,floors);
			Block originblock= elevator.getOrigin().getLocation().getBlock();
			p.setString(2, originblock.getX()+","+originblock.getZ());
			String world = elevator.getOrigin().getLocation().getWorld().getName();
			p.setString(3, world);
			p.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			if (connection!=null) {
				try {
				connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			
			if(p!=null) {
				try {
					p.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		
	}
	
	public void removeElevator(Elevator elevator){
		Connection connection = null;
		String remove = "DELETE FROM elevator WHERE origin=?" ;
        PreparedStatement p = null;
		
		try {
			Block origin=elevator.getOrigin().getLocation().getBlock();
			connection= hikari.getConnection();
			p =  connection.prepareStatement(remove);
			p.setString(1,origin.getX()+","+origin.getZ());
			p.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			if (connection!=null) {
				try {
				connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			
			if(p!=null) {
				try {
					p.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void loadfile(String path) {
		Connection connection = null;
		String update = "INSERT INTO elevator (world,origin,floors) VALUES (?,?,?)";
        PreparedStatement p = null;
		
		try {
			connection= hikari.getConnection();
			p =  connection.prepareStatement(update);
			
			Scanner scanner=null;
			try {
				scanner = new Scanner(new File(path));
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			while (scanner.hasNextLine()) {
				String[]data=scanner.nextLine().split(";");
				p.setString(1, data[0]);
				p.setString(2, data[1]);
				p.setString(3, data[2]);
				p.execute();
				
			}
			scanner.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			if (connection!=null) {
				try {
				connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			
			if(p!=null) {
				try {
					p.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
}
