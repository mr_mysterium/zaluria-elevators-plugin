package com.mr_mysterium.ElevatorPlugin;

import java.io.File;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import com.mr_mysterium.ElevatorPlugin.model.elevator.Elevator;
import com.mr_mysterium.ElevatorPlugin.model.elevator.Passenger;
import com.mr_mysterium.ElevatorPlugin.model.handler.DatabaseHandler;
import com.mr_mysterium.ElevatorPlugin.model.handler.EventHandler;



public class ZaluriaElevators extends JavaPlugin {
	
	private EventHandler eventhandler;
	private DatabaseHandler dbhandler;
	ArrayList<Elevator>elevators=Elevator.getElevators();
	HashMap<Player,Passenger>passengers=Passenger.getPassengers();
	FileConfiguration config = this.getConfig();
	

	
	@Override
	public void onEnable() {
		dbhandler= new DatabaseHandler(config);
		eventhandler= new EventHandler(dbhandler);
		this.getServer().getPluginManager().registerEvents(eventhandler, this);
		
		this.saveDefaultConfig();
		config.addDefault("Database.Address", "");
		config.addDefault("Database.Name", "");
		config.addDefault("Database.Username", "");
		config.addDefault("Database.Password", "");
		config.options().copyDefaults(true);
		saveConfig();
		
		dbhandler.connectToDatabase();
		dbhandler.loadElevators();
		
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (label.equalsIgnoreCase("loadelevatorfile")) {
			if (sender.isOp()) {
				dbhandler.loadfile(getDataFolder().getPath()+File.separator+"elevators.yml");
				sender.sendMessage(ChatColor.GREEN+"loaded elevator file, please restart the server to validate changes");
			}else {
				sender.sendMessage(ChatColor.RED+"You dont have the permission to do that");
			}
			
		}
		
		return true;
	}
	
	@Override
	public void onDisable() {
		for (Player player : Bukkit.getOnlinePlayers()) {
			if (passengers.containsKey(player)) {
				passengers.get(player).getFloorbar().removeAll();
			}
		}
	}
}
